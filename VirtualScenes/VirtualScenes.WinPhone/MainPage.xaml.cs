﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Sqlite;
using SQLite.Net.Platform.WindowsPhone8;
using Xamarin.Forms;


namespace VirtualScenes.WinPhone
{
    public partial class MainPage : PhoneApplicationPage
    {        
        public MainPage()
        {
            InitializeComponent();

            Forms.Init();
            
            var db = System.IO.Path.Combine(System.Environment.CurrentDirectory, "VirtualScenes.db");
            var app = new VirtualScenes.App(new SQLitePlatformWP8(), "Windows Phone", "1.0.0", db);
            Content = app.GetMainPage().ConvertPageToUIElement(this);
        }
    }
}
