﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using VirtualScenes.Commands;
using VirtualScenes.Contracts.Entities;
using VirtualScenes.Core;

namespace VirtualScenes.ViewModels
{
    public class CredentialViewModel : VirtualClientViewModel
    {
        private ISite _site;
        private string _status;
        private string _loginText;
        public delegate void LoginComplete();
        public event LoginComplete OnLoginComplete;

        public CredentialViewModel(VirtualClient virtualClient) : base(virtualClient)
        {
            _site = new Site()
            {
                Enabled = true,
                Password = "5757",
                Url = "http://10.0.0.236:8030"
            };
            this.LoginButtonText = "Login";
            virtualClient.SiteRepository.DefaultAsync().ContinueWith(c =>
            {
                
                if (c.IsCompleted && c.Result != null)
                {
                    var site = c.Result as ISite;
                    Site = site;
                }
            });
        }

        private ICommand loginCommand;
        public ICommand LoginCommand
        {
            get
            {
                if (loginCommand == null)
                    loginCommand = new LoginCommand(VirtualClient, this);

                (loginCommand as LoginCommand).OnLoginComplete += CredentialViewModel_OnLoginComplete;
                return loginCommand;
            }
            set
            {
                loginCommand = value;
            }
        }

        void CredentialViewModel_OnLoginComplete()
        {
            if (OnLoginComplete != null) OnLoginComplete();
        }

        public string Server
        {
            get { return this.Site.Url; }
            set { 
                this.Site.Url = value;
                OnPropertyChanged(); 
            }
        }

        public string Password
        {
            get
            {
                return this.Site.Password;                
            }
            set
            {
                this.Site.Password = value;
                OnPropertyChanged();
            }
        }

        public ISite Site
        {
            get
            {
                return _site;
            }
            set
            {
                _site = value;
                OnPropertyChanged();
            }
        }

        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                OnPropertyChanged();
            }
        }

        public string LoginButtonText
        {
            get { return _loginText; }
            set
            {
                _loginText = value;
                OnPropertyChanged();
            }
        }

    }
}