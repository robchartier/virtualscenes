﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using VirtualScenes.Core;
using VirtualScenes.Core.Entities;

namespace VirtualScenes.ViewModels
{
    public class DevicesViewModel : VirtualClientViewModel
    {

        public DevicesViewModel(VirtualClient virtualClient) : base(virtualClient)
        {
            this.Title = "devices";
            if (VirtualClient.DevicesResults != null && VirtualClient.DevicesResults.devices != null)
            {
                this.Devices = new ObservableCollection<Device>(VirtualClient.DevicesResults.devices.ToList());
            }
            else
            {
                this.Devices = new ObservableCollection<Device>();
            }
            VirtualClient.Client.Devices().ContinueWith(t =>
            {
                if (t.Result != null && t.IsCompleted)
                {
                    var result = (t.Result as DevicesResult);
                    if (result != null && result.devices != null)
                    {
                        foreach (var device in result.devices)
                        {
                            VirtualClient.Client.DeviceValues(device.id).ContinueWith(u =>
                            {
                                var dvr = (u.Result as DeviceValuesResult);
                                if (dvr.success)
                                {
                                    device.Values = dvr.values;
                                }
                                AddOrUpdate(device);

                            });
                        }
                    }
                }
            });
        }

        private void AddOrUpdate(Device device)
        {
            if (device != null)
            {
                if (Devices.LongCount() == 0)
                {
                    Devices.Add(device);
                }
                else
                {
                    var existing = (from d in Devices where d.id == device.id select d).FirstOrDefault();
                    if (existing==null)
                    {
                        Devices.Add(device);
                    }
                    else
                    {
                        existing.level = device.level;
                        existing.level_txt = device.level_txt;
                        existing.name = device.name;
                        existing.on_off = device.on_off;
                        existing.plugin_name = device.plugin_name;
                        existing.type = device.type;
                    }
                }
            }
        }

        private string _title;

        public ObservableCollection<Device> Devices
        {
            get;
            set;
        }

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged();
            }
        }

    }
}