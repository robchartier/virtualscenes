﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualScenes.Core;

namespace VirtualScenes.ViewModels
{
    public class VirtualClientViewModel : ViewModelBase
    {
        protected VirtualClient VirtualClient { get; set; }

        public VirtualClientViewModel(VirtualClient virtualClient)
        {
            VirtualClient = virtualClient;
        }
    }
}
