﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using VirtualScenes.Commands;
using VirtualScenes.Core;
using VirtualScenes.ViewModels;
using Xamarin.Forms;
using Button = Xamarin.Forms.Button;

namespace VirtualScenes.Pages
{
    public class CredentialPage : ContentPage
    {

        public delegate void LoginComplete();
        public event LoginComplete OnLoginComplete;

        private readonly VirtualClient _virtualClient;
        private Entry serverCell = null;
        private Entry passwordCell = null;

        private CredentialViewModel Context = null;

        public CredentialPage(VirtualClient virtualClient)
        {
            _virtualClient = virtualClient;
            Context = new CredentialViewModel(virtualClient);
            this.BindingContext = Context;
            Context.OnLoginComplete += Context_OnLoginComplete;
            
            Debug.WriteLine(this.GetType().FullName);
        }

        void Context_OnLoginComplete()
        {
            if (OnLoginComplete != null) OnLoginComplete();
        }

        protected override void OnAppearing()
        {

            var login = new Button();
            login.SetBinding(Button.TextProperty, "LoginButtonText");
            login.SetBinding(Button.CommandProperty, "LoginCommand");
            

            serverCell = new Entry();
            serverCell.SetBinding(Entry.TextProperty, "Site.Url");
            passwordCell = new Entry();
            passwordCell.SetBinding(Entry.TextProperty, "Site.Password");
            passwordCell.IsPassword = true;

            var failLabel = new Label() { Text = ""};
            failLabel.SetBinding(Label.TextProperty, "Status");


            var layout = new StackLayout()
            {
                Children =
                {
                    new Label() {Text = "Host"},
                    serverCell,
                    new Label() {Text = "Password"},
                    passwordCell,
                    login,
                    failLabel
                }
            };

            Content = layout;

            base.OnAppearing();
        }

    }
}