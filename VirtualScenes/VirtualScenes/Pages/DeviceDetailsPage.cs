﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using VirtualScenes.Core;
using VirtualScenes.Core.Entities;
using Xamarin.Forms;
using Device = VirtualScenes.Core.Entities.Device;

namespace VirtualScenes.Pages
{
    public class DeviceDetailsPage : ContentPage
    {

        private readonly VirtualClient _virtualClient;
        private DeviceProfile _device = null;

        public DeviceDetailsPage(VirtualClient virtualClient, DeviceProfile device)
            : base()
        {
            _virtualClient = virtualClient;
            _device = device;

            this.Title = "details";

            var layout = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                Padding = 20
            };
            var grid = new Grid
            {
                RowSpacing = 30,
                HorizontalOptions = new LayoutOptions(LayoutAlignment.Fill, true)
            };
            int row = 0;
            grid.Children.Add(new Label { Text = "Device: " }, 0, 0); //name
            grid.Children.Add(new Label { Text = _device.name }, 1, 0);  //value
            row++;
            grid.Children.Add(new Label { Text = "Level: " }, 0, row); //name
            grid.Children.Add(new Label { Text = _device.level_txt }, 1, row);  //value

            if (_device.Details != null && _device.Details.details != null)
            {
                if (!string.IsNullOrEmpty(_device.Details.details.cool_p))
                {
                    row++;
                    grid.Children.Add(new Label {Text = "Cool Point: "}, 0, row); //name
                    grid.Children.Add(new Label {Text = _device.Details.details.cool_p}, 1, row); //value
                }
                if (!string.IsNullOrEmpty(_device.Details.details.esm))
                {
                    row++;
                    grid.Children.Add(new Label {Text = "Esm: "}, 0, row); //name
                    grid.Children.Add(new Label {Text = _device.Details.details.esm}, 1, row); //value
                }
                if (!string.IsNullOrEmpty(_device.Details.details.fan_mode))
                {
                    row++;
                    grid.Children.Add(new Label {Text = "Fan Mode: "}, 0, row); //name
                    grid.Children.Add(new Label {Text = _device.Details.details.fan_mode}, 1, row); //value
                }
                if (!string.IsNullOrEmpty(_device.Details.details.fan_state))
                {
                    row++;
                    grid.Children.Add(new Label {Text = "Fan State: "}, 0, row); //name
                    grid.Children.Add(new Label {Text = _device.Details.details.fan_state}, 1, row); //value
                }
                if (!string.IsNullOrEmpty(_device.Details.details.heat_p))
                {
                    row++;
                    grid.Children.Add(new Label {Text = "Heat Point: "}, 0, row); //name
                    grid.Children.Add(new Label {Text = _device.Details.details.heat_p}, 1, row); //value
                }
                if (!string.IsNullOrEmpty(_device.Details.details.last_heard_from))
                {
                    row++;
                    grid.Children.Add(new Label {Text = "Last heard from: "}, 0, row); //name
                    grid.Children.Add(new Label {Text = _device.Details.details.last_heard_from}, 1, row); //value
                }
                if (!string.IsNullOrEmpty(_device.Details.details.mode))
                {
                    row++;
                    grid.Children.Add(new Label {Text = "Mode: "}, 0, row); //name
                    grid.Children.Add(new Label {Text = _device.Details.details.mode}, 1, row); //value
                }
                if (!string.IsNullOrEmpty(_device.Details.details.on_off))
                {
                    row++;
                    grid.Children.Add(new Label {Text = "On/Off: "}, 0, row); //name
                    grid.Children.Add(new Label {Text = _device.Details.details.on_off}, 1, row); //value
                }
                if (!string.IsNullOrEmpty(_device.Details.details.type_txt))
                {
                    row++;
                    grid.Children.Add(new Label {Text = "Type: "}, 0, row); //name
                    grid.Children.Add(new Label {Text = _device.Details.details.type_txt}, 1, row); //value
                }
            }

            layout.Children.Add(grid);
            Content = layout;

            Debug.WriteLine(this.GetType().FullName);
        }


    }
}