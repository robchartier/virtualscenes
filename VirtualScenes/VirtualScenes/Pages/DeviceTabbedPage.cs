﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using VirtualScenes.Core;
using VirtualScenes.Core.Entities;
using Xamarin.Forms;

namespace VirtualScenes.Pages
{
    public class DeviceTabbedPage : TabbedPage 
    {
        private readonly VirtualClient _virtualClient;
        private readonly DeviceProfile _deviceProfile;

        public DeviceTabbedPage(VirtualClient virtualClient, DeviceProfile deviceProfile)
        {
            _virtualClient = virtualClient;
            _deviceProfile = deviceProfile;

            this.Children.Add(new DeviceCommandsPage(_virtualClient, _deviceProfile));
            this.Children.Add(new DeviceDetailsPage(_virtualClient, _deviceProfile));

            Debug.WriteLine(this.GetType().FullName);
        }
    }
}
