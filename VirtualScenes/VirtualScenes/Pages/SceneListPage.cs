﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using VirtualScenes.Core;
using VirtualScenes.Core.Entities;
using Xamarin.Forms;

namespace VirtualScenes.Pages
{
    public class SceneListPage : ContentPage
    {

        private readonly VirtualClient _virtualClient;
        private ListView listView = null;
        public SceneListPage(VirtualClient virtualClient)
            : base()
        {
            _virtualClient = virtualClient;
            this.Title = "scenes";
            //this.IsVisible = false;

            listView = new ListView
            {
            };
            listView.ItemTemplate = new DataTemplate(() =>
            {
                var cell = new TextCell();
                cell.SetBinding<Scene>(TextCell.TextProperty, m => m.name);
                cell.SetBinding<Scene>(TextCell.CommandParameterProperty, m => m.id);
                cell.Tapped += cell_Tapped;
                return cell;
            });

            _virtualClient.Client.Scenes().ContinueWith(t =>
            {
                listView.ItemsSource = t.Result.scenes;
            }, TaskScheduler.FromCurrentSynchronizationContext());

            Content = new StackLayout
            {               
                VerticalOptions = LayoutOptions.FillAndExpand,
                Children = { listView }
            };

            Debug.WriteLine(GetType().FullName);
        }

        void cell_Tapped(object sender, EventArgs e)
        {
            var cell = sender as TextCell;
            var sceneId = cell.CommandParameter;
            if (sceneId != null)
            {
                _virtualClient.Client.StartScene((int)sceneId);
            }
        }

    }
}
