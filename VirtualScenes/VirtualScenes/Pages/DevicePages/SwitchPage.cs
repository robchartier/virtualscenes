﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using VirtualScenes.Contracts.Devices;
using VirtualScenes.Core;
using VirtualScenes.Core.Entities;
using Xamarin.Forms;
using Device = VirtualScenes.Core.Entities.Device;


namespace VirtualScenes.Pages.DevicePages
{
    public class SwitchPage : ContentPage
    {

        private readonly VirtualClient _virtualClient;
        private readonly VirtualScenes.Core.Entities.Device _device;
        private readonly ISwitch _switch;
        private string _value;
        public string Value
        {
            get { return _value; }
            set
            {
                _value = value;
                OnPropertyChanged();
            }
        }

        public SwitchPage(VirtualClient virtualClient, ISwitch device)
        {

            _virtualClient = virtualClient;
            _switch = device;
            _device = device as Device;

            Value = _switch.OnOff;
            System.Diagnostics.Debug.WriteLine(this.GetType().FullName);
        }

        private string _header = "";

        public string Header
        {
            get { return _header; }
            set
            {
                _header = value;
                OnPropertyChanged("Header");
            }
        }

        protected override void OnAppearing()
        {
            this.BindingContext = this;
            var header = new Label();
            header.SetBinding(Label.TextProperty, "Header");
            Header = _device.Display;

            var turnOn = new Button() {Text = "Turn On"};
            turnOn.Clicked += turnOn_Clicked;
            var turnOff = new Button() {Text = "Turn Off"};
            turnOff.Clicked += turnOff_Clicked;

            var valueLabel = new Label();
            valueLabel.SetBinding(Label.TextProperty, "Value");
            valueLabel.FontSize *= 4;
            valueLabel.HorizontalOptions = LayoutOptions.CenterAndExpand;


            var layout = new StackLayout()
            {
                Children =
                {
                    new Label() {Text = _device.Display},
                    new Label() {Text = _device.type},
                    valueLabel,
                    turnOn,
                    turnOff
                }
            };

            Content = layout;

            base.OnAppearing();
        }

        private void turnOff_Clicked(object sender, EventArgs e)
        {
            Value = "Off";
            (_switch as ISwitch).TurnOff().ContinueWith(t =>
            {
                Debug.WriteLine("Turn off Result:{0}", t.Result.ToString());
            });
        }

        private void turnOn_Clicked(object sender, EventArgs e)
        {
            Value = "On";
            (_switch as ISwitch).TurnOn().ContinueWith(t =>
            {
                Debug.WriteLine("Turn on Result:{0}", t.Result.ToString());
            });
        }

    }
}
