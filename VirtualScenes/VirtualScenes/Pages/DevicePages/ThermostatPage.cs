﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using VirtualScenes.Contracts.Devices;
using VirtualScenes.Contracts.Entities;
using VirtualScenes.Core;
using VirtualScenes.Core.Entities;
using Xamarin.Forms;
using Device = VirtualScenes.Core.Entities.Device;


namespace VirtualScenes.Pages.DevicePages
{
    public class ThermostatPage : ContentPage
    {

        private bool IsCelcius { get; set; }

        private readonly VirtualClient _virtualClient;
        private readonly IThermostat _thermostat;
        private readonly Device _device;

        private ITemperature _temperature = new Temperature(0);

        public ITemperature Temperature
        {
            get { return _temperature; }
            set
            {
                _temperature = value;
                OnPropertyChanged("Temperature");
            }
        }
        private ITemperature _changeTemperature = new Temperature(0);

        public ITemperature ChangeTemperature
        {
            get { return _changeTemperature; }
            set
            {
                _changeTemperature = value;
                OnPropertyChanged("ChangeTemperature");
            }
        }


        public ThermostatPage(VirtualClient virtualClient, IThermostat device)
        {
            _virtualClient = virtualClient;
            _device = device as Device;
            _thermostat = device;
            Temperature = _thermostat.Temperature;
            
            System.Diagnostics.Debug.WriteLine(this.GetType().FullName);
        }

        private string _header = "";
        public string Header
        {
            get { return _header; }
            set
            {
                _header = value;
                OnPropertyChanged("Header");
            }
        }

        private Slider slider = null;

        protected override void OnAppearing()
        {
            this.BindingContext = this;

            var header = new Label();
            header.SetBinding(Label.TextProperty, "Header");
            Header = _device.Display;

            slider = new Slider(0, 99, _thermostat.Temperature.Temp);
            slider.ValueChanged += slider_ValueChanged;
            
            var setHeatButton = new Button() {Text = "Set Heat Point"};
            setHeatButton.Clicked += setHeatButton_Clicked;

            var setCoolButton = new Button() {Text = "Set Cool Point"};
            setCoolButton.Clicked += setCoolButton_Clicked;

            var tempLabel = new Label();
            tempLabel.SetBinding(Label.TextProperty, "Temperature.Display");
            tempLabel.FontSize *= 4;
            tempLabel.HorizontalOptions = LayoutOptions.CenterAndExpand;

            var currentTempLabelText = new Label() {Text = "Current Temperature:"};
            var setTempLabelText = new Label() {Text = "Set Temperature:"};

            var setTempLabel = new Label();
            setTempLabel.SetBinding(Label.TextProperty, "ChangeTemperature.Display");
            ChangeTemperature = _thermostat.Temperature;
            setTempLabel.FontSize *= 3;
            setTempLabel.HorizontalOptions = LayoutOptions.CenterAndExpand;


            var layout = new StackLayout()
            {
                Children =
                {
                    new Label() {Text = _device.Display},
                    new Label() {Text = _device.type},
                    currentTempLabelText,
                    tempLabel,
                    setTempLabelText,
                    setTempLabel,
                    slider,
                    setHeatButton,
                    setCoolButton
                }
            };

            Content = layout;

            base.OnAppearing();
        }

        private Temperature SliderValue
        {
            get
            {
                return new Temperature(Math.Round(slider.Value, 0));
            }
        }

        void setHeatButton_Clicked(object sender, EventArgs e)
        {
            ChangeTemperature = SliderValue;
            _thermostat.SetHeatPoint(ChangeTemperature.Temp).ContinueWith(t => Debug.WriteLine("Set Heat Point Returned:{0}", t.Result.ToString()),TaskScheduler.FromCurrentSynchronizationContext());
        }
        void setCoolButton_Clicked(object sender, EventArgs e)
        {
            ChangeTemperature = SliderValue;
            _thermostat.SetCoolPoint(ChangeTemperature.Temp).ContinueWith(t => Debug.WriteLine("Set Cool Point Returned:{0}", t.Result.ToString()), TaskScheduler.FromCurrentSynchronizationContext());
            
        }

        void slider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            ChangeTemperature = SliderValue;
        }

    }
}
