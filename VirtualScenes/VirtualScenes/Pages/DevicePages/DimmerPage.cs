﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using VirtualScenes.Contracts.Devices;
using VirtualScenes.Core;
using VirtualScenes.Core.Entities;
using Xamarin.Forms;
using Device = VirtualScenes.Core.Entities.Device;


namespace VirtualScenes.Pages.DevicePages
{
    public class DimmerPage : ContentPage
    {

        private readonly VirtualClient _virtualClient;
        private readonly Device _device;
        private readonly IDimmer _dimmer;


        private int _value;
        public int Value
        {
            get { return _value; }
            set
            {
                _value = value;
                OnPropertyChanged("Value");
            }
        }

        public DimmerPage(VirtualClient virtualClient, IDimmer dimmer)
        {

            _virtualClient = virtualClient;
            _dimmer = dimmer;
            _device = dimmer as Device;
            Value = _dimmer.Level;
            
            System.Diagnostics.Debug.WriteLine(this.GetType().FullName);
        }

        private Slider slider = null;
        private string _header = "";

        public string Header
        {
            get { return _header; }
            set
            {
                _header = value;
                OnPropertyChanged("Header");
            }
        }

        
        protected override void OnAppearing()
        {

            var header = new Label();
            header.SetBinding(Label.TextProperty, "Header");
            Header = _device.Display;


            int value = 0;
            this.BindingContext = this;
            int.TryParse(_device.level, out value);

            slider = new Slider(0, 99, value);
            
            slider.ValueChanged += slider_ValueChanged;
            slider.SetBinding(Slider.IsEnabledProperty, "DimReady");
            
            var setLevelButton = new Button() { Text = "Set Level" };
            setLevelButton.Clicked += setLevelButton_Clicked;
            var turnOn = new Button() {Text = "Turn On"};
            turnOn.Clicked += turnOn_Clicked;
            var turnOff = new Button() {Text = "Turn Off"};
            turnOff.Clicked += turnOff_Clicked;

            var valueLabel = new Label();
            valueLabel.SetBinding(Label.TextProperty, "Value");
            valueLabel.FontSize *= 4;
            valueLabel.HorizontalOptions = LayoutOptions.CenterAndExpand;

            var layout = new StackLayout()
            {
                Children =
                {
                    new Label() {Text = _device.Display},
                    new Label() {Text = _device.type},
                    valueLabel,
                    slider,
                    setLevelButton,
                    turnOn,
                    turnOff
                }
            };

            Content = layout;

            base.OnAppearing();
        }

        void setLevelButton_Clicked(object sender, EventArgs e)
        {
            _dimmer.SetLevel((int)slider.Value).ContinueWith(t =>
            {
                Debug.WriteLine("Set Level Result:{0}", t.Result.ToString());
            });
        }

        private void turnOff_Clicked(object sender, EventArgs e)
        {
            slider.Value = slider.Minimum;
            (_dimmer as ISwitch).TurnOff().ContinueWith(t =>
            {
                Debug.WriteLine("Turn off Result:{0}", t.Result.ToString());
            });
        }

        private void turnOn_Clicked(object sender, EventArgs e)
        {
            slider.Value = slider.Maximum;
            (_dimmer as ISwitch).TurnOn().ContinueWith(t =>
            {
                Debug.WriteLine("Turn on Result:{0}", t.Result.ToString());
            });
        }



        private void slider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            Value = (int)Math.Round(e.NewValue, 0);
        }
    }
}