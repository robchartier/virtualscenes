﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using VirtualScenes.Core;
using VirtualScenes.Core.Entities;
using Xamarin.Forms;

namespace VirtualScenes.Pages
{
    public class LogPage : ContentPage
    {

        private readonly VirtualClient _virtualClient;
        private ListView listView = null;
        private LogResult _logResult;

        public LogPage(VirtualClient virtualClient)
            : base()
        {
            _virtualClient = virtualClient;
            this.Title = "logs";
            //this.IsVisible = false;

            this.BindingContext = this;
            listView = new ListView
            {
            };
            listView.BindingContext = this;
            listView.SetBinding(ListView.ItemsSourceProperty, "LogResult.logentries");
            
            listView.ItemTemplate = new DataTemplate(() =>
            {
                var cell = new TextCell();
                cell.SetBinding<Logentry>(TextCell.DetailProperty, m => m.Description);
                return cell;
            });
            _virtualClient.Client.LogEntries().ContinueWith(t =>
            {
                this.LogResult = t.Result as LogResult;
            }, TaskScheduler.FromCurrentSynchronizationContext());

            var refreshButton = new Button() {Text = "Refresh"};
            refreshButton.Clicked += refreshButton_Clicked;
            Content = new StackLayout
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                Children = {listView, refreshButton},                
            };
            Debug.WriteLine(GetType().FullName);
        }

        void refreshButton_Clicked(object sender, EventArgs e)
        {
            //LogResult.logentries = new Logentry[0];
            _virtualClient.Client.LogEntries().ContinueWith(t =>
            {
                LogResult = (t.Result as LogResult);
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        public LogResult LogResult
        {
            get { return _logResult; }
            set
            {
                _logResult = value;
                OnPropertyChanged("LogResult");
            }
        }
    }
}
