﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using VirtualScenes.Contracts.Devices;
using VirtualScenes.Core;
using VirtualScenes.Core.Entities;
using VirtualScenes.ViewModels;
using Xamarin.Forms;
using Device = VirtualScenes.Core.Entities.Device;


namespace VirtualScenes.Pages
{
    public class DevicesListPage : ContentPage
    {
        private readonly VirtualClient _virtualClient;

        private DevicesViewModel _context;

        private DevicesViewModel Context
        {
            get { return _context; }
            set
            {
                _context = value;
                this.BindingContext = value;
            }
        }

        private void Setup()
        {
            var list = new ListView();
            list.ItemsSource = Context.Devices;

            var layout = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                Padding = 10
            };
            list.ItemSelected += list_ItemSelected;
            list.ItemTemplate = new DataTemplate(() =>
            {
                Label nameLabel = new Label();
                nameLabel.SetBinding(Label.TextProperty, "Display");
                nameLabel.FontSize = nameLabel.FontSize + 7;

                return new ViewCell()
                {
                    View = new StackLayout()
                    {
                        Padding = new Thickness(0, 5),
                        Orientation = StackOrientation.Vertical,
                        HorizontalOptions = LayoutOptions.StartAndExpand,
                        Children =
                        {
                            nameLabel,
                        }
                    }
                };
            });
            layout.Children.Add(list);
            Content = layout;

        }



        private ListView list = new ListView();

        public DevicesListPage(VirtualClient virtualClient) : base()
        {
            _virtualClient = virtualClient;
            this.Context = new DevicesViewModel(virtualClient);
            this.Title = this.Context.Title;
            //list.SetBinding<DevicesViewModel>(ListView.ItemsSourceProperty, vm=>vm.Devices);
            Setup();
            _virtualClient.OnDevicesResult += _virtualClient_OnDevicesResult;
            Debug.WriteLine(this.GetType().FullName);
        }

        private void _virtualClient_OnDevicesResult(DevicesResult devicesResult)
        {
            this.Setup();
        }


        private void list_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var dev = e.SelectedItem as VirtualScenes.Core.Entities.Device;

            if ((dev as IThermostat) != null)
            {
                Navigation.PushAsync(new VirtualScenes.Pages.DevicePages.ThermostatPage(_virtualClient,
                    dev as IThermostat)); //.Wait();
            }
            else if ((dev as IDimmer) != null)
            {
                Navigation.PushAsync(new VirtualScenes.Pages.DevicePages.DimmerPage(_virtualClient, dev as IDimmer));
                    //.Wait();
            }
            else if ((dev as ISwitch) != null)
            {
                Navigation.PushAsync(new VirtualScenes.Pages.DevicePages.SwitchPage(_virtualClient, dev as ISwitch));
                    //.Wait();
            }

        }
    }
}