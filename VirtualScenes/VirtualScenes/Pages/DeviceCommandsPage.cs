﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using VirtualScenes.Core;
using VirtualScenes.Core.Entities;
using Xamarin.Forms;

namespace VirtualScenes.Pages
{
    public class DeviceCommandsPage : ContentPage
    {
        private string[] nonInputTypes = new string[]
        {
            "Turn On",
            "Turn Off"
        };
        private string[] removeList = new string[]
        {
            "library",
            "protocol",
            "application",
            "version",
            "test",
            "switch all",
            "ignore",
            "timeout",
            "frame",
            "report",
            "acked",
            "open",
            "close",
            "count",
            "powerlevel",
            "operating",
            "indicator"

        };

        private readonly VirtualClient _virtualClient;
        private DeviceProfile _device = null;

        public DeviceCommandsPage(VirtualClient client, DeviceProfile device)
            : base()
        {
            _virtualClient = client;
            _device = device;

            this.Title = device.name;


            var layout = new StackLayout
            {                
                Orientation = StackOrientation.Vertical,
                Padding = 10
            };
            var view = new TableView
            {
                HorizontalOptions = new LayoutOptions(LayoutAlignment.Fill, true),                
            };
            var section = new TableSection()
            {
                Title = "commands"
            };
            view.Root.Add(section);

            foreach (var cmd in KnownCommands())
            {
                Cell cell = null;

                if (cmd.friendlyname == "Turn On" /*|| cmd.friendlyname == "Turn On"*/)
                {
                    cell = new SwitchCell()
                    {                                                
                        Text = cmd.friendlyname,
                        On = (_device.level_txt == "On"),
                    };
                }
                else if(cmd.friendlyname == "Set Basic")
                {
                    cell = new EntryCell()
                    {
                        Text = _device.level_txt,
                        Label = cmd.friendlyname
                    };

                }
                if (cell != null)
                {
                    cell.PropertyChanged += cell_PropertyChanged;
                    section.Add(cell);
                }
            }
            

            layout.Children.Add(view);
            Content = layout;

            Debug.WriteLine(this.GetType().FullName);
        }

        void cell_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var friendlyName = "";
            var sw = (sender as SwitchCell);
            string value = "";
            if (sw != null)
            {
                friendlyName = sw.Text;
                value = sw.On.ToString();
            }
            else
            {
                var eCell = (sender as EntryCell);
                if (eCell != null)
                {
                    friendlyName = eCell.Label;
                    value = eCell.Text;
                }
            }
            if (!string.IsNullOrEmpty(friendlyName))
            {
                var cmd = (from c in KnownCommands() where c.friendlyname == friendlyName select c).FirstOrDefault();
                if (cmd != null)
                {
                    int arg = -1;
                    if (value == "true")
                    {
                        arg = 99;
                    }
                    else if (value == "false")
                    {
                        arg = 0;
                    }
                    else
                    {
                        int.TryParse(value, out arg);
                    }
                    if (arg >= 0)
                    {
                        Debug.WriteLine("Sending Device Command:{0}, {1}, {2}", _device.name, cmd.friendlyname, arg);
                        _virtualClient.Client.SendDeviceCommand(_device.id, cmd.name, arg, cmd.type);
                    }
                }
            }
        }

        private IEnumerable<Devicecommand> KnownCommands()
        {
            return _device.Commands.DeviceCommand.Where(e =>
            {
                foreach (var i in removeList)
                {
                    if (e.friendlyname.ToLower().Contains(i)) return false;
                }
                return true;
            });
        }

        private async void cell_Tapped(object sender, EventArgs e)
        {
            var commandId = (int) ((sender as TextCell).CommandParameter);
            var command = (from c in _device.Commands.DeviceCommand where c.id == commandId select c).FirstOrDefault();
            if (command != null)
            {
                if (command.name == "TURNOFF")
                {
                    //await _virtualClient.SendDeviceCommand(_device, command, 0);
                }
                else if (command.name == "TURNON")
                {
                    //await _virtualClient.SendDeviceCommand(_device, command, 99);
                }

            }

        }
    }
}