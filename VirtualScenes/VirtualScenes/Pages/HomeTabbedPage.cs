﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualScenes.Core;
using Xamarin.Forms;

namespace VirtualScenes.Pages
{
    public class HomeTabbedPage : TabbedPage
    {
        private readonly VirtualClient _virtualClient;

        public HomeTabbedPage(VirtualClient virtualClient)
        {
            _virtualClient = virtualClient;

            this.Children.Add(new DevicesListPage(_virtualClient));
            this.Children.Add(new SceneListPage(_virtualClient));
            this.Children.Add(new LogPage(_virtualClient));

        }


    }
}
