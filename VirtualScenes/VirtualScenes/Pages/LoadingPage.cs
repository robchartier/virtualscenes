﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualScenes.Core;
using Xamarin.Forms;

namespace VirtualScenes.Pages
{
    public class LoadingPage : ContentPage
    {
        private readonly VirtualClient _virtualClient;
        private int _progress;

        public LoadingPage(VirtualClient virtualClient)
        {

            _virtualClient = virtualClient;
            var layout = new StackLayout
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Spacing = 20
            };
            
            layout.Children.Add(new Image() {Source = "Images/32zvsIcon.png"});
            layout.Children.Add(new ActivityIndicator()
            {
                IsRunning = true
            });
            layout.Children.Add(new Label() {Text = "Loading...", HorizontalOptions = LayoutOptions.Center});
            Content = layout;
            Debug.WriteLine(this.GetType().FullName);

            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadHome();
        }

        private CredentialPage credentialPage = null;
        private async Task LoadHome()
        {
            credentialPage = new CredentialPage(_virtualClient);
            credentialPage.OnLoginComplete += credentialPage_OnLoginComplete;
            var login = false;
            var s = await _virtualClient.SiteRepository.DefaultAsync();
            
            if (s == null || string.IsNullOrEmpty(s.Url))
            {
                login = true;
            }
            else
            {
                _virtualClient.Client.Site = s as Site;
                var loginResult = await _virtualClient.Client.Login();
                if (loginResult.success)
                {
                    Debug.WriteLine("Login was a success, redirecting");
                    Navigation.PushAsync(new HomeTabbedPage(_virtualClient));//.Wait();
                }
                else
                {
                    login = true;
                }
            }
            if (login)
            {
                Navigation.PushAsync(credentialPage);//.Wait();
            }


        }

        void credentialPage_OnLoginComplete()
        {
            Navigation.PushAsync(new HomeTabbedPage(_virtualClient));
        }
    }
}

