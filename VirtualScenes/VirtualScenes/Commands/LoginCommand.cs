﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using VirtualScenes.Contracts.Entities;
using VirtualScenes.Core;
using VirtualScenes.Core.Entities;
using VirtualScenes.ViewModels;

namespace VirtualScenes.Commands
{
    public class LoginCommand : VirtualSceneCommand
    {
        public delegate void LoginComplete();
        public event LoginComplete OnLoginComplete;


        public LoginCommand(VirtualClient virtualClient, VirtualClientViewModel viewModel)
            : base(virtualClient, viewModel)
        {

        }

        public override void Execute(object parameter)
        {
            var credVM = (ViewModel as CredentialViewModel);
            ISite site = null;
            if (credVM != null)
            {
                credVM.Status = "Attempting connection...";
                site = credVM.Site;
                if (site != null)
                {
                    VirtualClient.Client.Site = site;
                    var loginResult = VirtualClient.Client.Login().ContinueWith(l =>
                    {
                        if (l.Status == TaskStatus.RanToCompletion)
                        {
                            var result = l.Result as LoginResult;
                            if (result != null)
                            {
                                if (result.success)
                                {
                                    credVM.Status = "Login was a success!";
                                    VirtualClient.SiteRepository.InsertAsync(site).ContinueWith(db =>
                                    {
                                        if (db.Status == TaskStatus.RanToCompletion)
                                        {
                                            credVM.Status = "I saved your credential in a safe place!";
                                        }
                                    });
                                }
                                else
                                {
                                    credVM.Status = "Login failed, try again later!";
                                }
                                if (OnLoginComplete != null) OnLoginComplete();
                            }
                        }
                    }, TaskScheduler.FromCurrentSynchronizationContext());
                }
            }
        }
    }
}