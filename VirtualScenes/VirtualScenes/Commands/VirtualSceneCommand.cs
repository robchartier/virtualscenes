﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using VirtualScenes.Core;
using VirtualScenes.ViewModels;

namespace VirtualScenes.Commands
{
    public abstract class VirtualSceneCommand : ICommand
    {
        protected VirtualClient VirtualClient;
        protected VirtualClientViewModel ViewModel;

        public VirtualSceneCommand(VirtualClient virtualClient, VirtualClientViewModel viewModel)
        {
            VirtualClient = virtualClient;
            ViewModel = viewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public abstract void Execute(object parameter);

        public event EventHandler CanExecuteChanged;

    }
}
