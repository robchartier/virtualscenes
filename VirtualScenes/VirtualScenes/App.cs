﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using VirtualScenes.Contracts.Data;
using VirtualScenes.Core;
using VirtualScenes.Core.Mapping;
using VirtualScenes.Data;
using VirtualScenes.Pages;
using Xamarin.Forms;

namespace VirtualScenes
{
    public class App
    {
        public static VirtualClient Client { get; set; }

        public App(SQLite.Net.Interop.ISQLitePlatform platform, string os, string version, string database = null)
        {
            IDatabase db = new Database(platform, database);
            db.InitializeAsync().Wait();
            var mapper = new AutoMapperMapper();
            mapper.Init();
            ISiteRepository siteRepo = new SiteRepository(db, mapper);

            Client = new VirtualClient(new Client(os, version), new Database(platform, database), siteRepo);
            Client.InitializeAsync().Wait();
            Debug.WriteLine("App, Database ready");
        }

        public Page GetMainPage()
        {
            //return new NavigationPage(new HomeTabbedPage(VirtualClient));
            return new NavigationPage(new LoadingPage(Client));
        }
    }
}
