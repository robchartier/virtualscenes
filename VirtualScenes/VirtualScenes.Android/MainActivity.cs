﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using SQLite.Net.Platform.XamarinAndroid;
using Xamarin.Forms.Platform.Android;
using Environment = System.Environment;

namespace VirtualScenes.Droid
{
    [Activity(Label = "VirtualScenes", MainLauncher = true)]
    public class MainActivity : AndroidActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Xamarin.Forms.Forms.Init(this, bundle);
            
            var db = System.IO.Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "VirtualScenes.db");
            if (!System.IO.Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)))
            {
                System.IO.Directory.CreateDirectory(
                    System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            }
            var app = new App(new SQLitePlatformAndroid(),"Android", "1.0.0", db);
            SetPage(app.GetMainPage());
        }
    }
}

