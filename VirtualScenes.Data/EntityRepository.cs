﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualScenes.Contracts.Data;
using VirtualScenes.Contracts.Mapping;

namespace VirtualScenes.Data
{
    public class EntityRepository<T> : IRepository<T> where T : new()
    {
        private readonly IDatabase _database;
        protected readonly IMapper _mapper;

        public EntityRepository(IDatabase database, IMapper mapper)
        {
            _database = database;
            _mapper = mapper;
        }

        public async Task CreateTableAsync()
        {
            await _database.CreateTableAsync<T>();
        }

        public async Task<int> InsertAsync(T entity)
        {
            return await _database.InsertAsync(entity);
        }

        public async Task<int> UpdateAsync(T entity)
        {
            return await _database.UpdateAsync(entity);
        }
        public async Task<T> FindByIdAsync(string id)
        {
            return await _database.FindByIdAsync<T>(id);
        }

        public async Task<List<T>> QueryAsync(string sql, params object[] args)
        {
            return await _database.QueryAsync<T>(sql, args);
        }

        public async Task<int> DeleteByIdAsync(string id)
        {
            return await _database.DeleteByIdAsync<T>(id);
        }
    }
}
