using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SQLite.Net;
using SQLite.Net.Async;
using SQLite.Net.Interop;
using VirtualScenes.Contracts.Data;


namespace VirtualScenes.Data
{
    public class Database : IDatabase
    {

        public Database(ISQLitePlatform platform, string databasePath)
        {
            this.DatabasePath = databasePath;
            _platform = platform;

        }

        public async Task Seed()
        {
            return;
        }

        public async Task CreateTableAsync<TEntity>() where TEntity : new()
        {
            await _dbConnection.CreateTableAsync<TEntity>();
        }

        public async Task<int> InsertAsync<TEntity>(TEntity entity)
        {
            return await _dbConnection.InsertAsync(entity);
        }

        public async Task<int> UpdateAsync<TEntity>(TEntity entity)
        {
            return await _dbConnection.UpdateAsync(entity);
        }

        public async Task<int> DeleteByIdAsync<TEntity>(string id)
        {
            return await _dbConnection.DeleteAsync<TEntity>(id);
        }

        public async Task<List<TEntity>> QueryAsync<TEntity>(string sql, params object[] args) where TEntity : new()
        {
            try
            {

                return await _dbConnection.QueryAsync<TEntity>(sql, args);
            }
            catch (Exception e)
            {
                
                throw;
            }
        }

        public async Task<TEntity> FindByIdAsync<TEntity>(string id) where TEntity : new()
        {
            return await _dbConnection.FindAsync<TEntity>(id);
        }

        public string DatabasePath { get; set; }
        private SQLiteAsyncConnection _dbConnection;
        private SQLiteConnectionString _connectionParameters;
        private SQLiteConnectionPool _sqliteConnectionPool;
        private ISQLitePlatform _platform;

        private SQLiteConnectionWithLock _connection;

        public async Task<bool> InitializeAsync()
        {
            try
            {

                _connectionParameters = new SQLiteConnectionString(DatabasePath, true);
                _sqliteConnectionPool = new SQLiteConnectionPool(_platform);
                _connection = _sqliteConnectionPool.GetConnection(_connectionParameters);
                _dbConnection = new SQLiteAsyncConnection(() => _connection);
            }
            catch (Exception e)
            {

                throw e;
            }



            //_dbConnection.SetConnection(_connection);

            //_dbConnection.SetPlatform(_platform);


            //_dbConnection.RegisterTypeMappingRead((t) =>
            //{
            //    return _container.GetType(t).GetType();
            //});
            //_dbConnection.RegisterTypeMappingWrite((t) =>
            //{
            //    return _container.GetType(t).GetType();
            //});


            //List<Task> tasks = new List<Task>();
            //tasks.Add(_dbConnection.CreateTableAsync<Plato.Core.Security.Account>());
            //tasks.Add(_dbConnection.CreateTableAsync<Item>());
            //tasks.Add(_dbConnection.CreateTableAsync<File>());
            //tasks.Add(_dbConnection.CreateTableAsync<ProviderData>());

            //Task.WhenAll(tasks.ToArray()).ContinueWith(t =>
            //{
            //    if (t.IsFaulted) throw t.Exception;


            //    var result = _dbConnection.CreateTableAsync<Company>().Result;
            //    result = _dbConnection.CreateTableAsync<EmailAddress>().Result;
            //    result = _dbConnection.CreateTableAsync<Address>().Result;
            //    result = _dbConnection.CreateTableAsync<Phone>().Result;

            //    result = _dbConnection.CreateTableAsync<Person>().Result;


            //    _log.Info("Add tables created");
            //    Seed().Wait();
            //}).Wait();


            return true;
        }

        public void Dispose()
        {
            try
            {
                _connection.Commit();
                _connection.Close();
                _connection.Dispose();

            }
            catch (Exception e)
            {

            }
        }
    }
}