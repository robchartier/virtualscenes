﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualScenes.Contracts.Data;
using VirtualScenes.Contracts.Mapping;
using VirtualScenes.Core;
using ISite = VirtualScenes.Contracts.Entities.ISite;

namespace VirtualScenes.Data
{
    public class SiteRepository : EntityRepository<Site>, ISiteRepository
    {
        public SiteRepository(IDatabase database, IMapper mapper) : base(database, mapper)
        {
            base.CreateTableAsync();
        }

        public async Task<ISite> DefaultAsync()
        {
            var all = await QueryAsync("select * from site", new object[] {});
            return (from s in all where s.Enabled == true select s).FirstOrDefault();
        }

        public async Task<int> InsertAsync(ISite entity)
        {
            return await base.InsertAsync(_mapper.Map<ISite, Site>(entity));
        }

        public async Task<int> UpdateAsync(ISite entity)
        {
            return await base.UpdateAsync(_mapper.Map<ISite, Site>(entity));
        }

        public async Task<ISite> FindByIdAsync(string id)
        {
            var entity = await base.FindByIdAsync(id);
            return _mapper.Map<Site, ISite>(entity);
        }

        public async Task<List<ISite>> QueryAsync(string sql, params object[] args)
        {
            List<Site> sites =  await base.QueryAsync(sql, args);
            if (sites != null)
            {
                return _mapper.Map<List<Site>, List<ISite>>(sites);
            }
            return default(List<ISite>);
        }

        public async Task<int> DeleteByIdAsync(string id)
        {
            return await base.DeleteByIdAsync(id);
        }
    }
}
