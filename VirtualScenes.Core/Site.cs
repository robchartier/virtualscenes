﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualScenes.Contracts.Entities;

namespace VirtualScenes.Core
{
    public class Site : ISite
    {

        public Site()
        {
            Id = System.Guid.NewGuid().ToString();
        }
        public string Id { get; set; }
        public string Url { get; set; }
        public string Password { get; set; }

        public bool Enabled { get; set; }
    }
}
