﻿using VirtualScenes.Contracts.Entities;
using VirtualScenes.Contracts.Mapping;

namespace VirtualScenes.Core.Mapping
{
    public class CoreMappingModule : IMappingModule
    {
        public void Load(IMapper manager)
        {            
            manager.CreateMap<ISite, Site>();
            
        }
    }
}
