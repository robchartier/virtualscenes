﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using VirtualScenes.Contracts.Mapping;

namespace VirtualScenes.Core.Mapping
{
    public class AutoMapperMapper : IMapper
    {
        public AutoMapperMapper()
        {
            _modules = new List<IMappingModule>()
            {
                new CoreMappingModule()
            };
        }
        private List<IMappingModule> _modules;

        public void Init()
        {
            foreach (var m in _modules)
            {
                m.Load(this);
            }
    
        }

        public D Map<S, D>(S source) where D : class
        {
            D result = (source as D);
            if (result != null) return result;
            return Mapper.Map<S, D>(source);
            
        }

        public void CreateMap<S, D>()
        {
            Mapper.CreateMap<S, D>();
        }

        public void CreateMap(Type S, Type D)
        {
            Mapper.CreateMap(S, D);
        }
    }
}
