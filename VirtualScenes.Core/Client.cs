﻿using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using VirtualScenes.Contracts.Entities;
using VirtualScenes.Core.Entities;
using VirtualScenes.Core.Entities.Devices;
using VirtualScenes.Core.Extensions;

namespace VirtualScenes.Core
{
    public class Client
    {
        private readonly string _os;
        private readonly string _version;

        public Client(string os, string version)
        {
            _os = os;
            _version = version;
        }

        private string ContentType = "application/json";

        private string FormatUrl(string resource, string queryString = null)
        {
            if (Site.Url.EndsWith("/")) Site.Url = Site.Url.Substring(0, Site.Url.Length - 1);
            var url = string.Format("{0}/api/{1}", Site.Url, resource);
            if (!string.IsNullOrEmpty(queryString))
            {
                url = string.Format("{0}?{1}", url, queryString);
            }
            return url;
        }

        #region log

        public async Task<LogResult> LogEntries()
        {
            return await GetResourceAsType<LogResult>("logentries");
        }

        #endregion

        #region Scenes

        public async Task<ScenesResult> Scenes()
        {
            return await GetResourceAsType<ScenesResult>("Scenes");
        }

        public async Task<StartSceneResult> StartScene(int sceneId)
        {
            return await PostResourceAsType<StartSceneResult>(string.Format("Scene/{0}", sceneId), "is_running=true");
        }


        #endregion

        #region Devices

        public async Task<SendCommandResult> SendDeviceCommand(int deviceId, string commandName, double commandArgument,
            string commandType)
        {

            if (commandArgument==-1)
            {
                return
                    await
                        PostResourceAsType<SendCommandResult>(string.Format("Device/{0}/command", deviceId),
                            string.Format("name={0}&type={1}", commandName, commandType));

            }
            else
            {
                return
                    await
                        PostResourceAsType<SendCommandResult>(string.Format("Device/{0}/command", deviceId),
                            string.Format("name={0}&arg={1}&type={2}", commandName, commandArgument, commandType));
                
            }
        }

        public async Task<DeviceDetailsResult> DeviceDetails(int deviceId)
        {
            return await GetResourceAsType<DeviceDetailsResult>(string.Format("Device/{0}", deviceId));
            
        }

        public async Task<DeviceValuesResult> DeviceValues(int deviceId)
        {
            return await GetResourceAsType<DeviceValuesResult>(string.Format("Device/{0}/Values", deviceId));
        }

        public async Task<DeviceCommandsResult> DeviceCommands(int deviceId)
        {
            return await GetResourceAsType<DeviceCommandsResult>(string.Format("Device/{0}/Commands", deviceId));
        }


        public async Task<DevicesResult> Devices()
        {
            var devices = await GetResourceAsType<DevicesResult>("Devices");
            var result = new DevicesResult();
            result.Message = devices.Message;
            result.StatusCode = devices.StatusCode;
            result.Success = devices.Success;
            result.success = devices.Success;
            result.devices = new Device[devices.devices.Length - 1];

            for (int x = 0; x < result.devices.Length; x++)
            {
                var d = devices.devices[x];
                d.Client = this;

                if (d.type == "THERMOSTAT")
                {
                    var thermo = new Thermostat();
                    thermo.Values = d.Values;
                    thermo.id = d.id;
                    thermo.level = d.level;
                    thermo.level_txt = d.level_txt;
                    thermo.name = d.name;
                    thermo.on_off = d.on_off;
                    thermo.plugin_name = d.plugin_name;
                    thermo.type = d.type;
                    thermo.Client = d.Client;
                    result.devices[x] = thermo;
                }
                else if (d.type == "SWITCH")
                {
                    var dev = new Switch();
                    dev.Values = d.Values;
                    dev.id = d.id;
                    dev.level = d.level;
                    dev.level_txt = d.level_txt;
                    dev.name = d.name;
                    dev.on_off = d.on_off;
                    dev.plugin_name = d.plugin_name;
                    dev.type = d.type;
                    dev.Client = d.Client;
                    result.devices[x] = dev;
                }
                else if (d.type == "DIMMER")
                {
                    var dev = new Dimmer();
                    dev.Values = d.Values;
                    dev.id = d.id;
                    dev.level = d.level;
                    dev.level_txt = d.level_txt;
                    dev.name = d.name;
                    dev.on_off = d.on_off;
                    dev.plugin_name = d.plugin_name;
                    dev.type = d.type;
                    dev.Client = d.Client;
                    result.devices[x] = dev;
                }
                else
                {
                    result.devices[x] = d;

                }
            }
            return result;
        }

        #endregion

        #region loginlogout

        public async Task<LogoutResult> Logout()
        {
            return await GetResourceAsType<LogoutResult>("Logout");
        }

        public bool IsLoggedIn { get; set; }

        public async Task<LoginResult> Login()
        {

            var loginResult =
                await
                    HttpClient.PostAsType<LoginResult>(FormatUrl("login"), string.Format("password={0}", Site.Password),
                        ContentType);

            IsLoggedIn = loginResult.Success;
            if (loginResult.Success)

            {
                if (!string.IsNullOrEmpty(loginResult.zvstoken) && loginResult.success)
                {
                    _client.DefaultRequestHeaders.Add("zvstoken", loginResult.zvstoken);
                }
            }
            return loginResult;
        }

        #endregion

        private async Task<T> GetResourceAsType<T>(string resource, string queryString = null)
        {
            Debug.WriteLine("GetResourceAsType {0}, {1}", resource, queryString);
            var result = await HttpClient.GetAsType<T>(FormatUrl(resource, queryString));
            Debug.WriteLine("GetResourceAsType {0}, {1} returned", resource, queryString);
            return result;
        }

        private async Task<T> PostResourceAsType<T>(string resource, string body = null)
        {
            Debug.WriteLine("PostResourceAsType {0}, {1}", resource, body);
            var result = await HttpClient.PostAsType<T>(FormatUrl(resource), body, ContentType);
            Debug.WriteLine("PostResourceAsType {0}, {1} returned", resource, result);
            return result;
        }

        public bool IsValid
        {
            get
            {
                if (!IsLoggedIn) return false;
                if (Site == null) return false;
                if (string.IsNullOrEmpty(Site.Password)) return false;
                if (string.IsNullOrEmpty(Site.Url)) return false;
                return Site.Enabled;
            }
        }

        public ISite Site { get; set; }
        private HttpClientHandler handler = new HttpClientHandler();
        private HttpClient _client = null;
        private object _clientLock = new object();

        private HttpClient HttpClient
        {
            get
            {
                lock (_clientLock)
                {
                    if (_client == null)
                    {
                        handler.CookieContainer = new CookieContainer();
                        _client = new HttpClient();
                        _client.DefaultRequestHeaders.UserAgent.TryParseAdd(UserAgent);
                        //_client.DefaultRequestHeaders.Add("Content-Type", ContentType);

                    }
                }
                return _client;
            }
        }

        private string UserAgent
        {
            //get { return string.Format("Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2;  WOW64; {0}/{1})", this._os, this._version); }
            get { return string.Format("zVirtualScenes {0}/{1}", this._os, this._version); }
        }


    }
}