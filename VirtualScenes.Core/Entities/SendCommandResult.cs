﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualScenes.Core.Entities
{
    public class SendCommandResult : BaseResult
    {
        public bool success { get; set; }
    }
}
