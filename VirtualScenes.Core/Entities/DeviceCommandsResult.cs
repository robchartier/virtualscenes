﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualScenes.Core.Entities
{
    public class DeviceCommandsResult : BaseResult
    {
        public bool success { get; set; }
        public Devicecommand[] DeviceCommand { get; set; }
    }

    public class Devicecommand
    {
        public int id { get; set; }
        public string type { get; set; }
        public string friendlyname { get; set; }
        public string helptext { get; set; }
        public string name { get; set; }
    }


}
