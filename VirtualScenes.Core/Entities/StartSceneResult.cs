﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualScenes.Core.Entities
{

    public class StartSceneResult : BaseResult
    {
        public bool success { get; set; }
        public string desc { get; set; }
    }

}