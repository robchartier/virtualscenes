﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualScenes.Contracts.Entities;

namespace VirtualScenes.Core.Entities
{
    public class Temperature : ITemperature
    {
        public double Temp { get; set; }
        public Temperature(double temp)
        {
            Temp = temp;
        }

        public string Display
        {
            get { return this.ToString(); }
        }
        public override string ToString()
        {
            if (System.Globalization.CultureInfo.CurrentUICulture.EnglishName.Contains("United States-"))
            {
                return Temp.ToString("N1") + "°F";
            }
            else
            {
                return Celcius(Temp).ToString("N1") + "°C";
            }
        }

        public static ITemperature FromCelcius(double c)
        {
            return new Temperature(Fahrenheit(c));
        }
        public static ITemperature FromFahrenheit(double f)
        {
            return new Temperature(f);
        }

        public double ToCelcius()
        {
            return Celcius(Temp);
        }

        static double Celcius(double f)
        {
            return 5.0 / 9.0 * (f - 32);
        }
        static double Fahrenheit(double c)
        {
            return  (c + 32) / 5.0 * 9.0;
        }
    }
}
