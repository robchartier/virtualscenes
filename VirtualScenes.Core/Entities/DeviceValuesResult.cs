﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualScenes.Core.Entities
{

    public class DeviceValuesResult : BaseResult
    {
        public bool success { get; set; }
        public Value[] values { get; set; }
    }

    public class Value
    {
        public string value_id { get; set; }
        public string value { get; set; }
        public string grene { get; set; }
        public string index2 { get; set; }
        public bool read_only { get; set; }
        public string label_name { get; set; }
        public int type { get; set; }
        public int id { get; set; }
    }


}