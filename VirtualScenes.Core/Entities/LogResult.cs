﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualScenes.Core.Entities
{


    public class LogResult : BaseResult
    {
        public bool success { get; set; }
        public Logentry[] logentries { get; set; }
    }


    public class Logentry
    {
        public DateTime Datetime { get; set; }
        public string DatetimeLog { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string Urgency { get; set; }
        public string Domain { get; set; }
        public object ExceptionObject { get; set; }
        public string Identity { get; set; }
        public string LocationFullInfo { get; set; }
        public string LocationLineNumber { get; set; }
        public string LocationMethodName { get; set; }
        public object MessageObject { get; set; }
        public string ThreadName { get; set; }
        public string UserName { get; set; }
        public int Id { get; set; }
    }



}