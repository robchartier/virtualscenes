﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualScenes.Contracts.Devices;
using VirtualScenes.Contracts.Entities;

namespace VirtualScenes.Core.Entities.Devices
{
    public class Thermostat : Device, IThermostat
    {
        private Devicecommand setHeatCommand = null;
        private Devicecommand setCoolCommand = null;
        
        public ITemperature Temperature
        {
            get
            {
                var temp = this.FindValueByName("Temperature").Result;
                double d = 0;
                double.TryParse(temp.value, out d);
                var t = new Temperature(d);
                return t;
            }
        }

        public async Task<bool> SetHeatPoint(double temperature)
        {
            if (setHeatCommand == null) setHeatCommand = await this.FindDeviceCommandByName("Set Heating 1");
            if (setHeatCommand != null)
            {
                var result = await Client.SendDeviceCommand(this.id, setHeatCommand.name, temperature, setHeatCommand.type);
                return result.success;
            }
            return false;
        }

        public async Task<bool> SetCoolPoint(double temperature)
        {
            if (setHeatCommand == null) setHeatCommand = await this.FindDeviceCommandByName("Set Cooling 1");
            if (setHeatCommand != null)
            {
                var result = await Client.SendDeviceCommand(this.id, setHeatCommand.name, temperature, setHeatCommand.type);
                return result.success;
            }
            return false;
        }

        public new string Display
        {
            get { return string.Format("{0} - {1}", this.name, Temperature); }
        }
    }
}
