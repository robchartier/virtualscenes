﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualScenes.Contracts.Devices;

namespace VirtualScenes.Core.Entities.Devices
{
    public class Switch : Device, ISwitch
    {


        public bool IsOn
        {
            get
            {
                var temp = this.FindValueByName("Switch").Result;
                if (temp == null) return false;
                bool d =false;
                bool.TryParse(temp.value, out d);
                return d;
            }
        }

        public string OnOff
        {
            get { return IsOn ? "On" : "Off"; }
        }


        private Devicecommand onCommand = null;
        private Devicecommand offCommand = null;
        public async Task<bool> TurnOn()
        {
            if (onCommand == null) onCommand = await this.FindDeviceCommandByName("Turn On");
            if (onCommand != null)
            {
                var result = await Client.SendDeviceCommand(this.id, onCommand.name, -1, onCommand.type);
                return result.success;
            }
            return false;
        }

        public async Task<bool> TurnOff()
        {
            if (offCommand == null) offCommand = await this.FindDeviceCommandByName("Turn Off");
            if (offCommand != null)
            {
                var result = await Client.SendDeviceCommand(this.id, offCommand.name, -1, offCommand.type);
                return result.success;
            }
            return false;
        }

        public new string Display
        {
            get { return string.Format("{0} - {1}", this.name, OnOff); }
        }
    }
}
