﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualScenes.Contracts.Devices;

namespace VirtualScenes.Core.Entities.Devices
{
    public class Dimmer : Switch, IDimmer
    {
        public int Level
        {
            get
            {
                var temp = this.FindValueByName("Basic").Result;
                if (temp == null) return 0;
                double d = 0;
                double.TryParse(temp.value, out d);
                return (int)d;
            }
        }


        private Devicecommand setLevelCommand = null;

        public async Task<bool> SetLevel(int level)
        {
            if (setLevelCommand == null) setLevelCommand = await this.FindDeviceCommandByName("Set Basic");
            if (setLevelCommand != null)
            {
                var result = await Client.SendDeviceCommand(this.id, setLevelCommand.name, (double)level, setLevelCommand.type);
                return result.success;
            }
            return false;
        }
        public new string Display
        {
            get
            {
                if (Level == 0) return string.Format("{0} - Off", this.name);
                if (Level == 99) return string.Format("{0} - On", this.name);
                return string.Format("{0} - {1}", this.name, Level);
            }
        }
    }
}
