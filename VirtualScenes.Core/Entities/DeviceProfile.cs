﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualScenes.Core.Entities
{
    public class DeviceProfile : Device
    {
        public DeviceValuesResult Values { get; set; }
        public DeviceDetailsResult Details { get; set; }
        public DeviceCommandsResult Commands { get; set; }
    }
}
