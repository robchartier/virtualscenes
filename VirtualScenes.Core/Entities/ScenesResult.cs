﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualScenes.Core.Entities
{

    public class ScenesResult : BaseResult
    {
        public bool success { get; set; }
        public Scene[] scenes { get; set; }
    }

    public class Scene
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool is_running { get; set; }
        public int cmd_count { get; set; }
    }

}