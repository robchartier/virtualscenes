﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualScenes.Core.Entities
{

    public class Device
    {
        public int id { get; set; }
        public string name { get; set; }
        public string on_off { get; set; }
        public string level { get; set; }
        public string level_txt { get; set; }
        public string type { get; set; }
        public string plugin_name { get; set; }

        public Client Client { get; set; }

        public async Task<Value> FindValueByName(string friendlyname)
        {
            if (this.Values == null)
            {
                var values = await Client.DeviceValues(this.id);
                if (values.success) this.Values = values.values;
            }
            if (this.Values == null) return null;
            foreach (var v in this.Values)
            {
                if (v.label_name == friendlyname) return v;
            }
            return null;
        }

        public async Task<Devicecommand> FindDeviceCommandByName(string friendlyname)
        {
            if (this.Commands == null)
            {
                var cmds = await Client.DeviceCommands(this.id);
                if (cmds.success) this.Commands = cmds.DeviceCommand;
            }
            if (this.Commands == null) return null;
            foreach (var c in this.Commands)
            {
                if (c.friendlyname == friendlyname) return c;
            }
            return null;
        }

        public string Display
        {
            get { return this.name; }
        }

        public Value[] Values { get; set; }
        public Devicecommand[] Commands { get; set; }
    }

}