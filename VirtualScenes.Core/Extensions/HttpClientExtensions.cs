﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using VirtualScenes.Core.Entities;

namespace VirtualScenes.Core.Extensions
{
    public static class HttpClientExtensions
    {

        public static async Task<T> GetAsType<T>(this HttpClient client, string Uri)
        {
            return await ConvertToType<T>(await client.GetAsync(Uri));
        }

        public static async Task<T> PostAsType<T>(this HttpClient client, string Uri, string data, string contentType)
        {
            return await ConvertToType<T>(await client.PostAsync(Uri, new StringContent(data, Encoding.UTF8, contentType)));
        }

        private static async Task<T> ConvertToType<T>(HttpResponseMessage message)
        {
            var resultContent = await message.Content.ReadAsStringAsync();
            T typeResult = default(T);
            if (message.IsSuccessStatusCode)
            {
                if (!string.IsNullOrEmpty(resultContent))
                {
                    typeResult = JsonConvert.DeserializeObject<T>(resultContent);
                }
                else
                {
                    typeResult = Activator.CreateInstance<T>();
                }
            }
            else
            {
                typeResult = Activator.CreateInstance<T>();
            }
            BaseResult baseResult = (typeResult as BaseResult);
            if (baseResult != null)
            {
                baseResult.Message = resultContent;
                baseResult.StatusCode = (int) message.StatusCode;
                baseResult.Success = message.IsSuccessStatusCode;
            }
            return typeResult;
        }

    }
}