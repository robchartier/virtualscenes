﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using VirtualScenes.Contracts.Data;
using VirtualScenes.Core.Entities;

namespace VirtualScenes.Core
{
    public class VirtualClient
    {
        public LogResult LogResults
        {
            get { return _logResults; }
            set
            {
                _logResults = value;
                if (OnLogResultsUpdated != null) OnLogResultsUpdated(_logResults);
            }
        }
        public delegate void LogResultUpdated(LogResult logResult);
        public event LogResultUpdated OnLogResultsUpdated;


        public ScenesResult ScenesResults
        {
            get { return _scenesResults; }
            set
            {
                _scenesResults = value;
                if (OnScenesResult != null) OnScenesResult(_scenesResults);
            }
        }

        public delegate void ScenesResultUpdated(ScenesResult scenesResult);
        public event ScenesResultUpdated OnScenesResult;

        public DevicesResult DevicesResults
        {
            get { return _devicesResults; }
            set
            {
                _devicesResults = value;
                if (OnDevicesResult != null) OnDevicesResult(_devicesResults);
            }
        }

        public delegate void DevicesResultUpdated(DevicesResult devicesResult);
        public event DevicesResultUpdated OnDevicesResult;

        public Dictionary<int, DeviceDetailsResult> DeviceDetailsResults
        {
            get { return _deviceDetailsResults; }
            set
            {
                _deviceDetailsResults = value;
                if (OnDeviceDetailsResults != null) OnDeviceDetailsResults(_deviceDetailsResults);
            }
        }

        public delegate void DeviceDetailsResultsUpdated(Dictionary<int, DeviceDetailsResult> deviceDetailsResults);
        public event DeviceDetailsResultsUpdated OnDeviceDetailsResults;

        public Dictionary<int, DeviceValuesResult> DeviceValuesResults
        {
            get { return _deviceValuesResults; }
            set
            {
                _deviceValuesResults = value;
                if (OnDeviceValuesResults != null) OnDeviceValuesResults(_deviceValuesResults);
            }
        }

        public delegate void DeviceValuesResultstUpdated(Dictionary<int, DeviceValuesResult> deviceValuesResults);
        public event DeviceValuesResultstUpdated OnDeviceValuesResults;

        public Dictionary<int, DeviceCommandsResult> DeviceCommandsResults
        {
            get { return _deviceCommandsResults; }
            set
            {
                _deviceCommandsResults = value;
                if (OnDeviceCommandsResults != null) OnDeviceCommandsResults(_deviceCommandsResults);
            }
        }

        public delegate void DeviceCommandsResultsUpdated(Dictionary<int, DeviceCommandsResult> deviceCommandsResults);
        public event DeviceCommandsResultsUpdated OnDeviceCommandsResults;


        public Client Client { get; set; }
        private IDatabase Database { get; set; }
        public ISiteRepository SiteRepository { get; set; }
        private LogResult _logResults;
        private ScenesResult _scenesResults;
        private DevicesResult _devicesResults;
        private Dictionary<int, DeviceDetailsResult> _deviceDetailsResults;
        private Dictionary<int, DeviceValuesResult> _deviceValuesResults;
        private Dictionary<int, DeviceCommandsResult> _deviceCommandsResults;

        public VirtualClient(Client client, IDatabase database, ISiteRepository siteRepository)
        {
            Client = client;
            Database = database;
            SiteRepository = siteRepository;
        }

        public async Task InitializeAsync()
        {            
        }

    }
}
