﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualScenes.Contracts.Mapping
{
    public interface IMappingModule
    {
        void Load(IMapper manager);
    }
}
