﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualScenes.Contracts.Mapping
{
    public interface IMapper
    {
        void Init();

        D Map<S, D>(S source) where D : class;
        void CreateMap<S, D>();
        void CreateMap(Type S, Type D);
    }
}
