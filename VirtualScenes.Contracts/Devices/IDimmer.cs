﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualScenes.Contracts.Devices
{
    public interface IDimmer
    {
        int Level { get; }
        Task<bool> SetLevel(int level);
    }
}
