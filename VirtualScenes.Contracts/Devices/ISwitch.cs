﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualScenes.Contracts.Devices
{
    public interface ISwitch
    {
        bool IsOn { get; }
        string OnOff { get; }
        Task<bool> TurnOn();
        Task<bool> TurnOff();
    }
}
