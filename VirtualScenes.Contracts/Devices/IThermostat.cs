﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualScenes.Contracts.Entities;

namespace VirtualScenes.Contracts.Devices
{
    public interface IThermostat
    {
        int id { get; }
        ITemperature Temperature { get; }
        Task<bool> SetHeatPoint(double temperature);
        Task<bool> SetCoolPoint(double temperature);
    }
}
