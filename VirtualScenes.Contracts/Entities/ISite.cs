﻿namespace VirtualScenes.Contracts.Entities
{
    public interface ISite
    {
        string Id { get; set; }
        string Url { get; set; }
        string Password { get; set; }
        bool Enabled { get; set; }
    }
}