﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualScenes.Contracts.Entities
{
    public interface ITemperature
    {
        double Temp { get; set; }
        double ToCelcius();
        string ToString();
    }
}
