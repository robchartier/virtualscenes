﻿namespace VirtualScenes.Contracts.Data
{
    public interface IProviderData
    {
        string Id { get; set; }
        string Payload { get; set; }
        
    }
}
