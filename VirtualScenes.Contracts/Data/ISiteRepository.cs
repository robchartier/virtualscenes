﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualScenes.Contracts.Entities;

namespace VirtualScenes.Contracts.Data
{
    public interface ISiteRepository : IRepository<ISite>
    {
        Task<ISite> DefaultAsync();

    }
}
