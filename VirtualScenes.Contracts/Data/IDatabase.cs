using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace VirtualScenes.Contracts.Data
{
    public interface IDatabase : IDisposable
    {
        Task<bool> InitializeAsync();
        Task<int> InsertAsync<TEntity>(TEntity entity);
        Task<int> UpdateAsync<TEntity>(TEntity entity);
        Task<int> DeleteByIdAsync<TEntity>(string id);
        Task<List<TEntity>> QueryAsync<TEntity>(string sql, params object[] args) where TEntity : new();
        Task<TEntity> FindByIdAsync<TEntity>(string id) where TEntity : new();
        Task CreateTableAsync<TEntity>() where TEntity : new();
        string DatabasePath { get; set; }
    }
}                                                                              
