using System.Collections.Generic;
using System.Threading.Tasks;

namespace VirtualScenes.Contracts.Data
{
    public interface IRepository<T>
    {
        Task CreateTableAsync();
        Task<int> InsertAsync(T entity);
        Task<int> UpdateAsync(T entity);
        Task<int> DeleteByIdAsync(string id);
        Task<List<T>> QueryAsync(string sql, params object[] args);
        Task<T> FindByIdAsync(string id);
        
    }
}
