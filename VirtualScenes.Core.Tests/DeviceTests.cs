﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace VirtualScenes.Core.Tests
{
    [TestFixture]
    public class DeviceTests : UnitTest
    {

        [Test]
        public void Devices()
        {
            SetDefaultCredentials();

            var login = Client.Client.Login().Result;
            if (login.Success)
            {
                var devices = Client.Client.Devices().Result;
                Assert.IsTrue(devices.Success);
            }
        }
        [Test]
        public void FirstDeviceCommands()
        {
            SetDefaultCredentials();

            var login = Client.Client.Login().Result;
            if (login.Success)
            {
                var devices = Client.Client.Devices().Result;
                if (devices.success)
                {
                    var first = devices.devices[0];
                    var details = Client.Client.DeviceCommands(first.id).Result;
                    Assert.IsTrue(details.Success);
                }
            }
        }

        [Test]
        public void FirstDeviceValues()
        {
            SetDefaultCredentials();
            var login = Client.Client.Login().Result;
            if (login.Success)
            {
                var devices = Client.Client.Devices().Result;
                if (devices.success)
                {
                    var first = devices.devices[0];
                    var details = Client.Client.DeviceValues(first.id).Result;
                    Assert.IsTrue(details.Success);
                }
            }
        }




        [Test]
        public void FirstDeviceFirstCommadn()
        {
            SetDefaultCredentials();
            var login = Client.Client.Login().Result;
            if (login.Success)
            {
                var devices = Client.Client.Devices().Result;
                if (devices.success)
                {
                    var first = devices.devices[0];
                    var details = Client.Client.DeviceCommands(first.id).Result;
                    var command = details.DeviceCommand[0];
                    var commandResult = Client.Client.SendDeviceCommand(first.id, command.name, 0, command.type).Result;
                    Assert.IsTrue(commandResult.Success);
                }
            }
        }
    }
}
