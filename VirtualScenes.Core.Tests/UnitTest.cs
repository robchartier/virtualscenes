﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using VirtualScenes.Contracts.Data;
using VirtualScenes.Data;

namespace VirtualScenes.Core.Tests
{
    public class UnitTest
    {
        [SetUp]
        public void SetUp()
        {
            var db = "VirtualScenes.db";
            IDatabase database = new Database(new SQLite.Net.Platform.Win32.SQLitePlatformWin32(), db);
            database.InitializeAsync().Wait();
            var mapper = new Mapping.AutoMapperMapper();
            mapper.Init();
            ISiteRepository siteRepo = new SiteRepository(database, mapper);
            _client = new VirtualClient(new Client("Unit Tests", "1.0.0"), database, siteRepo);
            _client.InitializeAsync().Wait();
            //_client.Site = Site;
        }

        protected void SetDefaultCredentials()
        {
            _client.Client.Site = Site;
        }

        public Site Site
        {
            get { return new Site() { Password = "8888", Url = "http://10.0.0.236:8030/" }; }
        }

        private VirtualClient _client = null;
        public VirtualClient Client
        {
            get
            {
                return _client;
           }
        }

    }
}