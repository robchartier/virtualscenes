﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace VirtualScenes.Core.Tests
{
    [TestFixture]
    public class AdminTests : UnitTest
    {

        [Test]
        public void AddSite()
        {
            var testSite = new Site()
            {
                Enabled = true,
                Password = "password",
                Url = "http://www.zvirtualscenes.com"
            };
            int count =0; //Client.Database.AddSite(testSite).Result;
            Assert.AreEqual(1, count);
        }

        //[Test]
        //public void GetSite()
        //{
        //    AddSite();
        //    var site = Client.Database.DefaultSite();
        //    Assert.AreEqual("password", site.Password);
        //    Assert.AreEqual(true, site.Enabled);
        //    Assert.AreEqual("http://www.zvirtualscenes.com", site.Url);
        //}

        [Test]
        public void Login()
        {
            SetDefaultCredentials();
            var success = Client.Client.Login().Result;
            Assert.IsTrue(success.success);
        }

        [Test]
        public void Logout()
        {
            SetDefaultCredentials();
            var success = Client.Client.Login().Result;
            if (success.success)
            {
                var logout = Client.Client.Logout().Result;
                Assert.IsTrue(logout.Success);
            }
        }


        [Test]
        public void Log()
        {
            SetDefaultCredentials();
            var login = Client.Client.Login().Result;
            if (login.Success)
            {
                var log = Client.Client.LogEntries().Result;
                Assert.IsTrue(log.success);
            }
        }


    }
}
