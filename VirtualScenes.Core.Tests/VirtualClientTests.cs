﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using VirtualScenes.Core.Entities;

namespace VirtualScenes.Core.Tests
{
    [TestFixture]
    public class VirtualClientTests : UnitTest
    {

        [Test]
        public void SyncDevices()
        {
            var virtualClient = new VirtualClient(base.Site);
            virtualClient.Devices.CollectionChanged += Devices_CollectionChanged;
            virtualClient.OnLoginFailed += virtualClient_OnLoginFailed;
            while (true)
            {
                System.Threading.Thread.Sleep(1000);
                if (devicesDone)
                {
                    break;
                }
            }

        }

        void Devices_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            devicesDone = true;
        }

        void virtualClient_OnLoginFailed(object sender, Entities.LoginResult result)
        {
            Assert.Fail("Login Failed. {0}", result.Message);
        }

        private List<Entities.DeviceProfile> _devices = null;
        private bool devicesDone = false;


        [Test]
        public void SyncScenes()
        {
            var client = new Client(base.Site);
            virtualClient.Scenes.CollectionChanged += Scenes_CollectionChanged;
            virtualClient.OnLoginFailed += virtualClient_OnLoginFailed;
            while (true)
            {
                System.Threading.Thread.Sleep(1000);
                if (scenesDone)
                {
                    Assert.IsNotNull(_scenes);
                    break;
                }
            }

        }

        void Scenes_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            scenesDone = true;
        }

        private List<Entities.Scene> _scenes = null;
        private bool scenesDone = false;

    }
}
