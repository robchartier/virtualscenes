﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace VirtualScenes.Core.Tests
{
    [TestFixture]
    public class ScenesTests : UnitTest
    {

        [Test]
        public void Scenes()
        {
            SetDefaultCredentials();
            var login = Client.Client.Login().Result;
            if (login.Success)
            {
                var scenes = Client.Client.Scenes().Result;
                Assert.IsTrue(scenes.Success);
            }
        }

        [Test]
        public void StartScene()
        {
            SetDefaultCredentials();
            var login = Client.Client.Login().Result;
            if (login.Success)
            {
                var scenes = Client.Client.Scenes().Result;
                var scene = scenes.scenes[3];
                var started = Client.Client.StartScene(scene.id).Result;

                Assert.IsTrue(scenes.Success);
            }            
        }
    }
}
